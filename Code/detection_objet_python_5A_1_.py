import cv2

# Initialization of the camera
capture = cv2.VideoCapture(0) #0 is the index if there are different cameras

# Screenshot of the first image from the camera
lecture_ok, first_image_array = capture.read() #lecture_ok is a boolean that indicates if the lecture of the frame did not encountered any problem, first_image_table is an array of the image

# Definition of the zone we are interested in : rectangle at the top of the image 
top = 0
bottom = 616  #616 pixels from the top > we detect 1/4 of the image, the total image is 2464 pixels
left = 0
right = first_image_array.shape[1] #width of the image


While True :
    # New screenshot of an image from the camera
    second_image_array = capture.read()

    # Zone we want to look at
    zone_first = first_image_array[top:bottom, left:right]
    zone_second = second_image_array[top:bottom, left:right]

    # Grayscale images
    gray_first = cv2.cvtColor(zone_first, cv2.COLOR_BGR2GRAY)
    gray_second = cv2.cvtColor(zone_second, cv2.COLOR_BGR2GRAY)

    # Difference between both images
    frame_diff = cv2.absdiff(gray_first, gray_second)

    # Threshold of the frame diff
    _, threshold = cv2.threshold(frame_diff, 30, 255, cv2.THRESH_BINARY)

    # Detection of the edges
    edges, _ = cv2.findContours(threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Check if there is some new edges in the specific zone
    if edges:
        print("Object at the top of the image !")

    # Update the next frame
    first_image_array = second_image_array

    # Exit the loop by pressing 'q'
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Ressources release
capture.release()
cv2.destroyAllWindows()


