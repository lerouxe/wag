# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 12:00:14 2023

@author: florin
"""
import serial
import RPi.GPIO as GPIO
import time
import threading
import math

def read_tfluna_data():
    """Main function to read data from LiDAR"""
    while True:
        counter = ser.in_waiting # count the number of bytes of the serial port
        if counter > 8 :
            bytes_serial = ser.read(9) # read 9 bytes
            ser.reset_input_buffer() # reset buffer

            if bytes_serial[0] == 0x59 and bytes_serial[1] == 0x59: # check first two bytes : Header
                distance = bytes_serial[2] + bytes_serial[3] * 256 # distance in next two bytes : Distance
                distance = distance / 100 # In meters
                return distance

#  *** To clean and creat GIPO ***
def setupGpio(gpioPinNum):
    '''Connection to the Haptic sensor'''
    GPIO.setmode(GPIO.BCM) # Broadcom pin-numbering scheme
    GPIO.setup(gpioPinNum, GPIO.OUT)
    return

def cleanupGpio():
    '''clear GPIO'''
    GPIO.cleanup()
    return

# *** GPIO PWM Mode Setup and PWM Output ***
def setGpioPinPwmMode(gpioPinNum, frequency):
    pwmPinObject = GPIO.PWM(gpioPinNum, frequency)
    return pwmPinObject

def pwm_ChangeFrequency(pwmPinObject, frequency):
    pwmPinObject.ChangeFrequency(frequency)
    return

def pwm_ChangeDutyCycle(pwmPinObject, dutyCycle):
    pwmPinObject.ChangeDutyCycle(dutyCycle)
    return

#  *** GPIO PWM Start and Stop ***
def PWM_Start(pwm_Object, initDutyCycle):
    pwm_Object.start(initDutyCycle)
    return

def Pwm_Stop(pwmPinObject):
    pwmPinObject.stop()
    return

def switch_vibration_mode(Object, duration, dutyCycle):
    """Function to switch vibration mode"""
    #DC = frequency/10 # duty cycle
    #pwm_ChangeFrequency(Object, frequency)
    pwm_ChangeDutyCycle(Object, dutyCycle)
    time.sleep(duration) # do the pwm for time of duration

def DutyCycle(distance) :
    """ Calculates the frequency according to distance
    Minimum distance were you feel : 3 m
    Minimum dc is 0
    Maximum dc possible is 100 wich means that 1000 Hz """
    if distance <= 3 :
        dutyCycle =  100 - 30 * distance # calculate the frequency
    else : 
        dutyCycle = 0
    return dutyCycle

def Duration_vib(frequency) :
    """Calculate the duration of the vibration"""
    #distance=-3*math.log(frequency/1000)
    #duration = 0.1+math.sqrt(distance/75)
    duration = 0.05
    return duration

# MAIN at the begining

# Connection to the LiDAR
ser = serial.Serial(
    port='/dev/serial0',
    baudrate=115200,
    timeout=0
)
print('  Begin setPwm, ...')

gpioPinNum   =   4 # Pin for GPIO
frequency    =   1000 # Frequency at the beginning
dutyCycle    =   0 # Duty Cycle at the beginning

# Set GPIO
setupGpio(gpioPinNum)

# Creat PWM OBJECT
Object = setGpioPinPwmMode(gpioPinNum, frequency) # Set first Mode
PWM_Start(Object, dutyCycle) # Start PWM


print('  End   setPwm, ...\r\n')

try:
    while True:
        distance = read_tfluna_data() #read the information of the LiDAR
        #print("Distance from sensor : " + str(distance) + " m")

        # Calculates the Intensity felt according to distance
        DC = DutyCycle(distance)
        #print(DC)

        # Calculate the duration
        duration = Duration_vib(DC)
        #print(duration)

        # Create the vibration
        switch_vibration_mode(Object, duration, DC)


except KeyboardInterrupt:          # trap a CTRL+C keyboard interrupt
    pass

finally:
    Object.stop()
    cleanupGpio()               # resets all GPIO ports used by this program
